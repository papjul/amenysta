<?php

declare(strict_types=1);

namespace App\Tests;

use Generator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class ApplicationAvailabilityFunctionalTest.
 */
class ApplicationAvailabilityFunctionalTest extends WebTestCase
{
    /**
     * @dataProvider urlProvider
     */
    public function testPageIsSuccessful(string $url)
    {
        $client = static::createClient();
        $client->request('GET', $url);

        $this->assertResponseIsSuccessful();
    }

    /**
     * @dataProvider urlRedirectProvider
     */
    public function testPageRedirects(string $url)
    {
        $client = static::createClient();
        $client->request('GET', $url);

        $this->assertResponseRedirects();
    }

    /**
     * @return Generator
     */
    public function urlRedirectProvider(): Generator
    {
        yield ['/contact'];
        yield ['/portfolio'];
        yield ['/cv'];

        yield ['/site/learnmore'];

        yield ['/admin/contact'];
    }

    /**
     * @return Generator
     */
    public function urlProvider(): Generator
    {
        yield ['/'];

        // Projets
        yield ['/projects/amenysta-world'];
        yield ['/projects/black-org'];
        yield ['/projects/movemo'];

        // Articles
        yield ['/articles'];
        yield ['/site/amenysta'];
        yield ['/tuto/game_design'];
        yield ['/tuto/gameplay'];
        yield ['/tuto/gentoo'];
        yield ['/tuto/write'];
    }
}
