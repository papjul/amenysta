<?php

namespace App\Form;

use App\Validator\Constraints\NoNumberSixTimes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Blank;

class ContactForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('identity', TextType::class, [
                'label' => 'Identité',
                'constraints' => [new NoNumberSixTimes()],
                'required' => true,
                'attr' => ['placeholder' => 'Nom sous lequel apparaître', 'maxlength' => 50],
            ])
            ->add('email', EmailType::class, [
                'label' => 'Adresse',
                'required' => true,
                'attr' => ['placeholder' => 'Courriel pour vous recontacter', 'maxlength' => 50],
            ])
            ->add('subject', TextType::class, [
                'label' => 'Objet',
                'required' => true,
                'attr' => ['placeholder' => 'Objet de votre message', 'maxlength' => 50],
            ])
            ->add('message', TextareaType::class, [
                'label' => 'Message',
                'required' => true,
                'attr' => ['placeholder' => 'Votre message', 'rows' => 5],
            ])
            ->add('content', TextareaType::class, [
                'attr' => ['class' => 'honeypot'],
                'constraints' => [new Blank()], // Honeypot
                'required' => false,
                'mapped' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
