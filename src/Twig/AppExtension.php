<?php

declare(strict_types=1);

namespace App\Twig;

use App\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

/**
 * Class AppExtension.
 */
class AppExtension extends AbstractExtension
{
    /** @var EntityManagerInterface */
    private EntityManagerInterface $entityManager;

    /**
     * AppExtension constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return array
     */
    public function getFilters(): array
    {
        return [];
    }

    /**
     * @return array
     */
    public function getFunctions(): array
    {
        return [
            new TwigFunction('get_pending_contact_messages_count', [$this->entityManager->getRepository(Contact::class), 'getPendingsCount']),
        ];
    }

    /**
     * Returns the canonical name of this helper.
     *
     * @return string The canonical name
     *
     * @api
     */
    public function getName(): string
    {
        return 'layout';
    }
}
