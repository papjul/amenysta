<?php

namespace App\Controller;

use App\Entity\Contact;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class AdminController.
 */
#[Route(path: '/admin')]
#[IsGranted("ROLE_SUPER_ADMIN")]
class AdminController extends AbstractController
{
    /**
     *
     * @param EntityManagerInterface $entityManager
     * @return Response
     */
    #[Route(path: '/contact', methods: 'GET', name: 'admin_contact')]
    public function contact(EntityManagerInterface $entityManager): Response
    {
        $messages = $entityManager->getRepository(Contact::class)->findAll();

        return $this->render('admin/contact.html.twig', ['messages' => $messages]);
    }
}
