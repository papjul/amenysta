<?php

namespace App\Controller;

use App\Entity\Contact;
use App\Form\ContactForm;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Controller used to manage portfolio part of the site.
 */
class MainController extends AbstractController
{
    /**
     *
     * @param Request $request
     * @param EntityManagerInterface $entityManager
     * @param MailerInterface $mailer
     * @return RedirectResponse|Response
     * @throws TransportExceptionInterface
     */
    #[Route(path: '/', methods: 'GET|POST', name: 'homepage')]
    public function homepage(Request $request, EntityManagerInterface $entityManager, MailerInterface $mailer): RedirectResponse|Response
    {
        $contact = new Contact();
        $form = $this->createForm(ContactForm::class, $contact);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            /*# Protection anti-flood
            $date  = time();
            $ip    = get_ip();
            $flood = 10 * 60; // 10 min

            $req1 = Engine::$db->prepare('SELECT c_identity
                                          FROM   '.CONTACT_TABLE.'
                                          WHERE  c_date > :date
                                          AND    c_ip = :ip');
            $req1->bindValue(':date', intval($date - (10 * 60)));
            $req1->bindValue(':ip',   $ip);
            $req1->execute();

            if($req1->rowCount() > 0)
              $this->setTpl('contact.failed');

            else
            {
                //continue
            }*/
            $contactFormData = $form->getData();
            $contactFormData->setCreationDate(new DateTime());
            $entityManager->persist($contactFormData);
            $entityManager->flush();

            // TODO: It should be sent once a day via a daily cron
            $message = (new Email())
                ->from($this->getParameter('email_from'))
                ->to($this->getParameter('email_to'))
                ->subject('[Amenysta] Nouveau message de contact')
                ->text('Un nouveau message est disponible sur amenysta.net.')
            ;
            $mailer->send($message);
            $this->addFlash('success', 'Votre message a bien été envoyé.');

            return $this->redirectToRoute('homepage', ['_fragment' => 'contact']);
        }

        return $this->render('homepage', ['form' => $form->createView()]);
    }

    #[Route(path: '/projects/{name}', methods: 'GET', name: 'project_view', requirements: ['name' => 'movemo|black-org|amenysta-world'])]
    public function projectView(string $name): Response
    {
        if ('movemo' === $name) {
            return $this->render('projects/movemo');
        } elseif ('black-org' === $name) {
            return $this->render('projects/black-org');
        } elseif ('amenysta-world' === $name) {
            return $this->render('projects/amenysta-world');
        }
    }

    #[Route(path: '/articles', methods: 'GET', name: 'articles')]
    public function articlesIndexView(): Response
    {
        return $this->render('articles');
    }

    #[Route(path: '/{type}/{name}', methods: 'GET', name: 'article_view_tuto', requirements: ['type' => 'tuto', 'name' => 'gentoo|game_design|gameplay|write'])]
    #[Route(path: '/{type}/{name}', methods: 'GET', name: 'article_view_site', requirements: ['type' => 'site', 'name' => 'learnmore|amenysta|mentions-legales'])]
    public function articleView(string $type, string $name): Response
    {
        if ('tuto' === $type) {
            if ('gentoo' === $name) {
                return $this->render('articles/gentoo');
            } elseif ('game_design' === $name) {
                return $this->render('articles/game_design');
            } elseif ('gameplay' === $name) {
                return $this->render('articles/gameplay');
            } elseif ('write' === $name) {
                return $this->render('articles/write');
            }
        } elseif ('site' === $type) {
            if ('amenysta' === $name) {
                return $this->render('articles/amenysta');
            } elseif ('learnmore' === $name) {
                return $this->redirectToRoute('project_view', ['name' => 'amenysta-world']);
            } else if ('mentions-legales' === $name) {
                return $this->render('articles/mentions-legales');
            }
        }
    }

    #[Route(path: '/portfolio', methods: 'GET', name: 'portfolio')]
    public function portfolioView(): RedirectResponse
    {
        return $this->redirectToRoute('homepage', ['_fragment' => 'projects']);
    }

    #[Route(path: '/cv', methods: 'GET', name: 'cv')]
    public function cvView(): RedirectResponse
    {
        return $this->redirectToRoute('homepage', ['_fragment' => 'about']);
    }

    #[Route(path: '/contact', methods: 'GET')]
    public function contact(): RedirectResponse
    {
        return $this->redirectToRoute('homepage', ['_fragment' => 'contact']);
    }
}
