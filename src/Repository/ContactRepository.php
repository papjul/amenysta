<?php

namespace App\Repository;

use App\Entity\Contact;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;

/**
 * Class ContactRepository.
 */
class ContactRepository extends ServiceEntityRepository
{
    /**
     * ContactRepository constructor.
     *
     * @param ManagerRegistry $registry
     */
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contact::class);
    }

    /**
     * @return Contact|null
     *
     * @throws NonUniqueResultException
     */
    public function getOldestContactMessageWithIp(): ?Contact
    {
        /*$qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')
            ->from(Contact::class, 'c', 'c.id')
            ->where('c.ip IS NOT NULL')
            ->addOrderBy('c.creationDate', 'ASC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();*/
        return null;
    }

    /**
     * @return Contact|null
     *
     * @throws NonUniqueResultException
     */
    public function getOldestContactMessageWithEmail(): ?Contact
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('c')
            ->from(Contact::class, 'c', 'c.id')
            ->where('c.email IS NOT NULL')
            ->addOrderBy('c.creationDate', 'ASC')
            ->setMaxResults(1);

        return $qb->getQuery()->getOneOrNullResult();
    }

    /**
     * @return int
     *
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    public function getPendingsCount(): int
    {
        $qb = $this->getEntityManager()->createQueryBuilder();
        $qb->select('COUNT(c)')
            ->from(Contact::class, 'c', 'c.id');
        //->where('c.validated = false')
        //->andWhere('c.deleted = false');

        return intval($qb->getQuery()->getSingleScalarResult());
    }
}
