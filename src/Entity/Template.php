<?php

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Template.
 */
#[ORM\Table]
#[ORM\Entity]
class Template
{
    /**
     * @var int
     */
    #[ORM\Column(type: 'integer', nullable: false, options: ['unsigned' => true])]
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'IDENTITY')]
    protected int $id;

    /**
     * @var string
     */
    #[Assert\NotBlank]
    #[Assert\Length(min: 2, max: 50)]
    #[ORM\Column(type: 'string', length: 50, nullable: false)]
    protected string $name;

    /**
     * @var string
     */
    #[Assert\NotBlank]
    #[ORM\Column(type: 'text', nullable: false)]
    protected string $content;

    /**
     * @var DateTime
     */
    #[ORM\Column(type: 'datetime', nullable: false)]
    protected DateTime $lastUpdated;

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Template
     */
    public function setName(string $name): Template
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

    /**
     * @param string $content
     * @return Template
     */
    public function setContent(string $content): Template
    {
        $this->content = $content;

        return $this;
    }

    public function getLastUpdated(): DateTime
    {
        return $this->lastUpdated;
    }

    public function setLastUpdated(DateTime $lastUpdated): Template
    {
        $this->lastUpdated = $lastUpdated;

        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }
}
